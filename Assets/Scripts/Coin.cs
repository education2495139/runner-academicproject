using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    [SerializeField] private Transform _coinTransform;

    private void Start()
    {
        _coinTransform.rotation = Quaternion.Euler(0f, Random.Range(0, 180), 0f);
    }

    private void Update()
    {
        _coinTransform.Rotate(0, 100f * Time.deltaTime, 0);
    }
}
