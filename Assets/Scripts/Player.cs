using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Transform ScreenCanvas;
    public GameObject GameOverScreenPref;
    public float DistanceBetweenRoads;
    public RoadSpawner _roadSpawner;
    public GameObject AddCoinPrefab;

    [HideInInspector] public int CurrentPosition = 0;

    private const float moveSpeedFactor = 10f;
    private const float jumpHeigthFactor = 6f;
    private Collider _collider;
    private Rigidbody _rigidbody;
    private bool _isJumping = false;

    private Coroutine MoveRoutine;

    private void Start()
    {
        _collider = GetComponent<Collider>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        MoveUpdate();
        JumpUpdate();
    }

    private void MoveUpdate()
    {
        var moveAxis = Input.GetAxis("Horizontal");
        
        if (moveAxis == 0 || MoveRoutine != null)
        {
            return;
        }

        var moveDirection = moveAxis < 0 ? 1 : -1;

        MoveRoutine = StartCoroutine(ToMoveSide(moveDirection, 1f));

        // var moveQuant = moveAxis * moveSpeedFactor;
        //_rigidbody.MovePosition(new Vector3(_rigidbody.position.x + moveAxis, _rigidbody.position.y, _rigidbody.position.z));

        //_rigidbody.velocity = new Vector3(moveAxis * moveSpeedFactor, _rigidbody.velocity.y, _rigidbody.velocity.z);
    }

    private IEnumerator ToMoveSide(int moveDirection, float time)
    {
        var offsetPosition = moveDirection * DistanceBetweenRoads;
        if (Mathf.Abs(CurrentPosition + offsetPosition) <= 3)
        {
            var startPosition = transform.position;
            var targetPosition = transform.position;
            targetPosition.z += offsetPosition;

            var timer = 0f;
            //while (Mathf.Abs(transform.position.z - moveDirection * DistanceBetweenRoads) > 0.1f)
            while (timer < time)
            {
                transform.position = Vector3.Lerp(startPosition, targetPosition, timer);

                timer += Time.deltaTime;

                yield return new WaitForEndOfFrame();
            }

            transform.position = targetPosition;
            CurrentPosition += moveDirection;
        }

        //yield return new WaitForEndOfFrame();

        MoveRoutine = null;
    }

    private void JumpUpdate()
    {
        if (_isJumping)
        {
            return;
        }

        var isJumpPressed = Input.GetAxis("Vertical") > 0;
        if (isJumpPressed && !_isJumping)
        {
            _rigidbody.AddForce(new Vector3(0, jumpHeigthFactor * 10, 0), ForceMode.Impulse);
            _isJumping = true;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.parent.gameObject.TryGetComponent<Road>(out _))
        {
            _roadSpawner.Spawn();
        }

        if (collider.transform.parent.gameObject.TryGetComponent<Coin>(out var coin))
        {
            Destroy(coin.gameObject);
            GameController.Instance.currentScore += 10;

            var addScore = Instantiate(AddCoinPrefab, coin.transform.position, Quaternion.identity);

            StartCoroutine(ToShowAddScore(addScore));
        }
    }

    private IEnumerator ToShowAddScore(GameObject addScore)
    {
        yield return new WaitForSeconds(0.2f);
        Destroy(addScore);
    }

    private void OnCollisionEnter(Collision collision)
    {
        foreach (var contact in collision.contacts)
        {
            if (contact.otherCollider.gameObject.tag == "Ground")
            {
                _isJumping = false;
            }

            if (contact.otherCollider.gameObject.tag == "Enemy")
            {
                var gameOverScreen = Instantiate(GameOverScreenPref, ScreenCanvas);
                Time.timeScale = 0;
            }
        }
    }
}
