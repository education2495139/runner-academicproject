using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeadlyForce : MonoBehaviour
{
    private const float moveSpeedFactor = 1f;
    private Collider _collider;
    private Rigidbody _rigidbody;

    void Start()
    {
        _collider = GetComponent<Collider>();
        //_rigidbody = GetComponent<Rigidbody>();
        
    }

    void FixedUpdate()
    {
        MoveForward();
    }

    private void MoveForward()
    {
        var moveQuant = moveSpeedFactor * Time.fixedDeltaTime;
        transform.position += new Vector3(moveQuant, 0f, 0f);
        //_rigidbody.MovePosition(new Vector3(_rigidbody.position.x + moveQuant, _rigidbody.position.y, _rigidbody.position.z));
    }
}
