using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;

    private float _startDistance;

    private void Start()
    {
        _startDistance = Vector3.Distance(_playerTransform.position, transform.position);
    }

    void Update()
    {
        PositionUpdate();
    }

    private void PositionUpdate()
    {
        var position = transform.position;
        position.x = _playerTransform.position.x - _startDistance;
        transform.position = position;
    }

}
