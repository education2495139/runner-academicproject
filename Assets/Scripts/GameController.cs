using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private GameObject Player;
    [SerializeField] private Button restartButton;

    private GameObject[] objectsArray;
    private int currentEnemy = 0;

    public int currentScore = 0;
    private float timer = 0f;

    public static GameController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Init();

        restartButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
            Time.timeScale = 1f;
        });
    }

    void Update()
    {
        if (timer > 1f)
        {
            timer = 0f;
            Time.timeScale += 0.01f;
            currentScore++;
        }

        timer += Time.deltaTime;

        scoreText.text = currentScore.ToString();
    }

    private void Init()
    {
        objectsArray = GameObject.FindGameObjectsWithTag("Enemy")
            .Where(item => item.name != "EnemyDeadlyForce")
            .OrderBy(item => item.transform.position.x)
            .ToArray();
    }

    private void ScoreCheck()
    {
        if (currentEnemy > objectsArray.Length - 1)
        {
            return;
        }

        if (Player.transform.position.x > objectsArray[currentEnemy].transform.position.x)
        {
            currentEnemy++;

            scoreText.text = currentEnemy.ToString();
        }
    }
}
