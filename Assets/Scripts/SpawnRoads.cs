using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class RoadSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> _roads;
    [SerializeField] private List<float> _roadLenth;
    private GameObject _road;
    private void Start() 
    {
        _road = Instantiate(_roads[Random.Range(0, _roads.Count)], transform);
    }

    public void Spawn() 
    {
        var index = Random.Range(0, _roads.Count);
        var pos = _road.transform.position;
        pos.x += _roadLenth[index];
        Vector3 position = new Vector3(transform.position.x + _roadLenth[index], transform.position.y, transform.position.z);
        _road = Instantiate(_roads[index], pos, Quaternion.identity, transform);
    }
}
